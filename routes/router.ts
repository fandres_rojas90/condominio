import {Router} from 'express';
import { UserController } from '../controller/usercontroller';
import { RolController } from '../controller/rolController';

const router = Router();

/*
* Router UsuarioController
*/
router.get('/user',UserController.getuser);
router.get('/user/:id',UserController.getuserbyId);
router.post('/user',UserController.postUser);

/*
* Router RolController
*/
router.post('/rol',RolController.postrol);

export default router;