import {Request,Response} from 'express';
import User from '../models/user';

export class UserController {

    userStore  = User;

    constructor() {
        this.userStore  = [];
    }

    public static getuser(req: Request, res: Response) {
        res.json({
            'ok': true,
            'msg': "correcto",
            'std': 1
        });
    }

    public static getuserbyId(req: Request, res: Response) {
        let id = req.params.id;
        res.json({
            'ok': true,
            'msg': "correcto",
            'std': id
        });
    }

    public static postUser(req: Request, res: Response){
        let data = req.body;
        const newUser = new User(data.name,data.username,data.email);
        res.json({
            'ok': true,
            'msg': "correcto",
            'std': 1,
            'data': newUser
        });
    }

}