export default class User {
    id?: number;
    username: String;
    name: String;
    email: String;

    constructor(name: String, username: String, email: String, id?: number) {
      this.id = id;
      this.name = name;
      this.username = username;
      this.email = email;
    }

    getUsername() {
      return this.username;
    }
    getName() {
      return this.name;
    }

}